import argparse

from flask import Flask, jsonify

parser = argparse.ArgumentParser(description="service")
parser.add_argument("--port", default=5000)
parser.add_argument("--secret-number", type=str, required=True)

args = parser.parse_args()

PORT = args.port
SECRET_NUMBER = args.secret_number

app = Flask(__name__)


@app.route('/return_secret_number')
def return_secret_number():
    return jsonify({'secret_number': SECRET_NUMBER})


@app.route('/status')
def return_status():
    return jsonify({'status': 'test'})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=PORT)
